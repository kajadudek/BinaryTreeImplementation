package org.example;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TreeTest {
    private Tree createExampleTree1(){
        Node n1 = new Node(5);
        Node n2 = new Node(3);
        Node n3 = new Node(2);
        Node n4 = new Node(5);
        Node n5 = new Node(7);
        Node n6 = new Node(1);
        Node n7 = new Node(0);
        Node n8 = new Node(2);
        Node n9 = new Node(8);
        Node n10 = new Node(5);
        n1.setLeftChild(n2);
        n1.setRightChild(n5);
        n2.setLeftChild(n3);
        n2.setRightChild(n4);
        n5.setLeftChild(n6);
        n5.setRightChild(n7);
        n7.setLeftChild(n8);
        n7.setRightChild(n9);
        n9.setRightChild(n10);

        return new Tree(n1);
    }

    private Tree createExampleTree2(){
        Node n1 = new Node(5);
        Node n2 = new Node(3);
        Node n3 = new Node(2);
        Node n4 = new Node(5);
        Node n5 = new Node(7);
        Node n6 = new Node(1);
        Node n7 = new Node(0);
        n1.setLeftChild(n2);
        n2.setLeftChild(n3);
        n3.setLeftChild(n4);
        n4.setLeftChild(n5);
        n5.setLeftChild(n6);
        n6.setRightChild(n7);

        return new Tree(n1);
    }

    private Tree createExampleTree3(){
        Node n1 = new Node(5);
        Node n2 = new Node(3);
        Node n3 = new Node(2);
        Node n4 = new Node(5);
        Node n5 = new Node(7);
        Node n6 = new Node(1);
        Node n7 = new Node(0);
        Node n8 = new Node(2);
        Node n9 = new Node(8);
        Node n10 = new Node(5);
        n1.setLeftChild(n2);
        n1.setRightChild(n8);
        n2.setLeftChild(n3);
        n2.setRightChild(n9);
        n3.setLeftChild(n4);
        n3.setRightChild(n10);
        n4.setLeftChild(n5);
        n5.setLeftChild(n6);
        n6.setRightChild(n7);

        return new Tree(n1);
    }


    @Test
    public void calculateLeafsTest(){
        Tree tree1 = createExampleTree1();
        Tree tree2 = createExampleTree2();
        Tree tree3 = createExampleTree3();
        Tree tree4 = new Tree(null);
        Tree tree5 = new Tree(new Node(10));

        assertEquals(5, tree1.calculateLeafs(tree1.getRootNode()));
        assertEquals(1, tree2.calculateLeafs(tree2.getRootNode()));
        assertEquals(4, tree3.calculateLeafs(tree3.getRootNode()));
        assertEquals(0, tree4.calculateLeafs(tree4.getRootNode()));
        assertEquals(1, tree5.calculateLeafs(tree5.getRootNode()));
    }

    @Test
    public void longestPath(){
        Tree tree1 = createExampleTree1();
        Tree tree2 = createExampleTree2();
        Tree tree3 = createExampleTree3();
        Tree tree4 = new Tree(null);
        Tree tree5 = new Tree(new Node(10));

        assertEquals(5, tree1.longestPathInTree(tree1.getRootNode()));
        assertEquals(7, tree2.longestPathInTree(tree2.getRootNode()));
        assertEquals(7, tree3.longestPathInTree(tree3.getRootNode()));
        assertEquals(0, tree4.longestPathInTree(tree4.getRootNode()));
        assertEquals(1, tree5.longestPathInTree(tree5.getRootNode()));
    }

    @Test
    public void equalsTest(){
        Tree tree1 = createExampleTree1();
        Tree tree2 = createExampleTree2();
        Tree tree3 = createExampleTree3();
        Tree tree4 = new Tree(null);
        Tree tree5 = new Tree(null);
        Tree tree6 = createExampleTree1();

        assertTrue(tree1.equals(tree1));
        assertTrue(tree1.equals(tree6));
        assertTrue(tree4.equals(tree5));

        assertFalse(tree1.equals(tree2));
        assertFalse(tree2.equals(tree3));
        assertFalse(tree4.equals(tree2));
    }
}