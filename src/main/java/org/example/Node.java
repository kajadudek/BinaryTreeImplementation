package org.example;

public class Node {
    private final int value;
    private Node leftChild = null;
    private Node rightChild = null;

    public Node(int value){
        this.value = value;
    }
    public void setLeftChild(Node node){
        this.leftChild = node;
    }
    public void setRightChild(Node node){
        this.rightChild = node;
    }

    public Node getLeftChild(){
        return leftChild;
    }

    public Node getRightChild(){
        return rightChild;
    }

    public int getValue(){
        return this.value;
    }
}
