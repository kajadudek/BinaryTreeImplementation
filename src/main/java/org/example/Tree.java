package org.example;

public class Tree {

    private Node rootNode;

    public Tree(Node rootNode){
        this.rootNode = rootNode;
    }

    public Node getRootNode(){
        return this.rootNode;
    }

    public int calculateLeafs(Node rootNode){
        if (rootNode == null){
            return 0;
        }

        if (rootNode.getLeftChild() == null && rootNode.getRightChild() == null){
            return 1;
        }

        return calculateLeafs(rootNode.getRightChild()) + calculateLeafs(rootNode.getLeftChild());
    }

    public int longestPathInTree(Node rootNode) {
        if (rootNode == null) {
            return 0;
        }

        if (rootNode.getRightChild() == null && rootNode.getLeftChild() == null){
            return 1;
        }

        return Math.max(longestPathInTree(rootNode.getRightChild()) + 1,
                longestPathInTree(rootNode.getLeftChild()) + 1);
    }

    @Override
    public boolean equals(Object tree){
        if (this == tree) {
            return true;
        }
        if (!(tree instanceof Tree otherTree)){
            return false;
        }

        if ((otherTree.getRootNode() != null && this.rootNode == null)
                || (otherTree.getRootNode() == null && this.rootNode != null)){
            return false;
        }

        return (otherTree.getRootNode() == null && this.rootNode == null) || (
                otherTree.getRootNode().getValue() == this.rootNode.getValue()
                && new Tree(otherTree.rootNode.getLeftChild()).equals(new Tree(this.rootNode.getLeftChild()))
                && new Tree(otherTree.rootNode.getRightChild()).equals(new Tree(this.rootNode.getRightChild())));
    }
}
