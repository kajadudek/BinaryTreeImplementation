package org.example;

public class Main {
    public static void main(String[] args) {
        // Create tree
        Node n1 = new Node(5);
        Node n2 = new Node(3);
        Node n3 = new Node(2);
        Node n4 = new Node(5);
        Node n5 = new Node(7);
        Node n6 = new Node(1);
        Node n7 = new Node(0);
        Node n8 = new Node(2);
        Node n9 = new Node(8);
        Node n10 = new Node(5);
        n1.setLeftChild(n2);
        n1.setRightChild(n5);
        n2.setLeftChild(n3);
        n2.setRightChild(n4);
        n5.setLeftChild(n6);
        n5.setRightChild(n7);
        n7.setLeftChild(n8);
        n7.setRightChild(n9);
        n9.setRightChild(n10);

        Tree binTree = new Tree(n1);
        Tree binTree2 = new Tree(n1);

        System.out.println(binTree.calculateLeafs(n1));
        System.out.println(binTree.longestPathInTree(n1));
        System.out.println(binTree.equals(binTree2));
    }
}