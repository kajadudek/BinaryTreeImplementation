# BinaryTreeImplementation
- An implementation of binary tree structure in Java
- Program allows us to create binary tree, check number of its leaves, find the length of the longest path and check if two trees are equal.
- There are unit tests created for methods mentioned above.
